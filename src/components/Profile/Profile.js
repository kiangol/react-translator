import {useDispatch, useSelector} from "react-redux";
import {sessionLogoutAction} from "../../store/actions/sessionActions";
import AppContainer from "../../hoc/AppContainer";
import ProfileHistory from "./ProfileHistory"
import {Redirect} from "react-router-dom";

export const Profile = () => {
    const {loggedIn, username} = useSelector(state => state.sessionReducer)
    const dispatch = useDispatch()

    const logout = () => {
        dispatch(sessionLogoutAction())
    }

    const deletePosts = () => {
        console.log("Posts deleted")
    }

    return (
        <>
        {!loggedIn && <Redirect to="/login"/>}
        <AppContainer>
            <ProfileHistory />

            <h4>Welcome {username}</h4>
            {/*<>*/}
            {/*    <h2>Translation history:</h2>*/}
            {/*    <ul className="list-group">*/}
            {/*    {history.map(function(name, index) {*/}
            {/*        return <li className="list-group-item" key={index}>{name}</li>*/}
            {/*    })}*/}
            {/*    </ul>*/}
            {/*</>*/}
            <button className="btn btn-warning btn-sm" onClick={logout}>Log out</button>
            <button className="btn btn-info btn-sm" onClick={deletePosts}>Clear History</button>
        </AppContainer>
        </>
    )

}

export default Profile