export const ProfileAPI = {
    getLatestTranslations(user) {
        const { token } = JSON.parse(localStorage.getItem('translateusr-ss'))
        return fetch (`http://localhost:3242/users/${user}`)
            .then(async response => {
                if(!response.ok) {
                    throw new Error('Could not fetch user')
                }
                console.log("PROFILE_API: " + response.body)
                return response.json()
            })
    }

}