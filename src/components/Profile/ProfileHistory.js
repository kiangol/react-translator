import {useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";
import {userFetchingAction} from "../../store/actions/userActions";

const ProfileHistory = () => {
    const { userHistory } = useSelector(state => state.userReducer)
    const { username } = useSelector(state => state.sessionReducer)
    const dispatch = useDispatch()

    useEffect(() => {
        console.log("USERNAME: " + username)
        if (username) {
            dispatch(userFetchingAction(username))
        }
    }, [dispatch, username])

    return (
        <>
            <h4>Your history</h4>
            <ul className="list-group">
                {userHistory.map(function(name, index) {
                    return <li className="list-group-item" key={index}>{name}</li>
                })}
            </ul>
        </>
    )
}

export default ProfileHistory