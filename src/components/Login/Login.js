import AppContainer from '../../hoc/AppContainer';
import {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {loginAttemptAction} from "../../store/actions/loginActions";
import {Redirect} from "react-router-dom";

const Login = () => {

    const dispatch = useDispatch()
    const {loginError, loginAttempting} = useSelector(state => state.loginReducer)
    const {loggedIn} = useSelector(state => state.sessionReducer)

    const [credentials, setCredentials] = useState({
        username: '',
        history: ['this', 'is', 'awesome']
    })

    const onInputChange = event => {
        setCredentials({
            ...credentials,
            [event.target.id]: event.target.value
        })
    }

    const onFormSubmit = event => {
        event.preventDefault() // Avoid page reload
        dispatch(loginAttemptAction(credentials))
    }

    return (
        <>
            {loggedIn && <Redirect to="/translate"/>}
            {!loggedIn &&
            <AppContainer>
                <form className="mt-3 mb-3" onSubmit={onFormSubmit}>
                    <h2>Login to Translator</h2>
                    <p>Get started</p>

                    <div className="mb-3">
                        <label htmlFor="username" className="form-label">Name</label>
                        <input id="username" type="text" placeholder="What is your name?" onChange={onInputChange}/>
                    </div>

                    <button type="submit" className="btn btn-primary btn-lg">Login</button>
                </form>

                {loginAttempting &&
                <p>Logging in </p>
                }

                {loginError &&
                <div className="alert alert-danger" role="alert">
                    <h4>Unsuccessful login 🥵</h4>
                    <p className="mb-0">{loginError}</p>
                </div>
                }

            </AppContainer>
            }
        </>

    )
}

export default Login