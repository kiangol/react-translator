export const LoginAPI = {
    login(credentials) {
        return fetch(`http://localhost:3242/users?username=${credentials.username}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            },
        })
            .then(async (response) => {
                const res = await response.json()
                if (res.length === 1) {
                    return res
                } else {
                    throw new Error("USER NOT EXIST")
                }
            })
    },

    register(credentials) {
        return fetch(`http://localhost:3242/users`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(credentials)
        })
            .then(async (response) => {
                if (!response.ok) {
                    // Status was not 200, 201, ...
                    const {error = 'Error occurred with API request'} = await response.json()
                    throw new Error(error)
                }
                console.log("RESPONSE: " + response.body)
                return response.json()
            })
    }
}