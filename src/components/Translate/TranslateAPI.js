export const TranslateAPI = {
    addTranslation(user, entry) {
        const { token } = JSON.parse(localStorage.getItem('translateusr-ss'))
        return fetch (`http://localhost:3242/users/${user}`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(entry)
        })
            .then(async response => {
                if(!response.ok) {
                    throw new Error('Could not add translation')
                }
                return response.json()
            })
    }
}