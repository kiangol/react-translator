import {useDispatch, useSelector} from 'react-redux'
import AppContainer from "../../hoc/AppContainer";
import {sessionLogoutAction} from '../../store/actions/sessionActions'
import {Redirect} from "react-router-dom";
import {useState} from "react";
import {addTranslateAttemptAction} from "../../store/actions/translateActions";


export const Translate = () => {
    const {loggedIn, username} = useSelector(state => state.sessionReducer)
    const dispatch = useDispatch()

    const logout = () => {
        dispatch(sessionLogoutAction())
    }

    const [translateStr, setHistory] = useState({
        body: '',
        display: false
    })

    const onInputChange = e => {
        setHistory({
            ...translateStr,
            display:false,
            [e.target.id]: e.target.value
        })
    }

    const onFormSubmit = e => {
        e.preventDefault() // Avoid page reload
        dispatch(addTranslateAttemptAction(translateStr))
        setHistory({
            body: translateStr.body,
            display: true
        })
    }


    return (
        <>
            {!loggedIn && <Redirect to="/"/>}
            {loggedIn &&
            <AppContainer>
                <nav>
                    <a href="/profile">Profile</a>
                </nav>
                <h1>Translate</h1>
                <form className="mb-3" onSubmit={onFormSubmit}>
                    <div className="mb-3">
                        <label htmlFor="body" className="form-label visually-hidden" aria-label="Enter text">Enter text</label>
                        <textarea id="body" onChange={ onInputChange }
                                  rows="2"
                                  className="form-control"
                                  placeholder="Enter text"
                                  value={ translateStr.body }
                        />
                    </div>

                    {/*<label htmlFor="translateString" className="form-label">*/}
                    {/*    <span className="material-icons">translate</span>*/}
                    {/*</label>*/}

                    {/*<input id="translateString" type="text" placeholder="Translate"/>*/}
                    <button type="submit" className="btn btn-primary btn-sm">Translate</button>
                </form>


                <div className="alert alert-success" role="alert">
                    {!translateStr.display &&
                    <p>Hit translate!</p>}

                    {translateStr.display &&
                    translateStr.body.split('').map(function(name, index) {
                        let ref = `./images/individual_signs/${name}.png`
                        return <img src={ref} alt={name}/>
                    })}

                </div>



                <button onClick={logout}>LOGOUT</button>

            </AppContainer>
            }
        </>
    )
}

export default Translate