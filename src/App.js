import './App.css';
import {
    BrowserRouter,
    Switch,
    Route,
    Redirect
} from "react-router-dom";
import Login from "./components/Login/Login";
import NotFound from "./components/NotFound/NotFound";
import Translate from "./components/Translate/Translate";
import Profile from "./components/Profile/Profile";

function App() {
    return (
        <BrowserRouter>
            <div className="App">
                <nav className="navbar navbar-dark bg-dark">
                    <div className="container-fluid">
                    <a href="/" className="navbar-brand">Translatr</a>
                        <section className="d-flex">
                            <a href="/profile" className="navbar-brand">Profile</a>
                        </section>
                    </div>
                </nav>
                <Switch>
                    <Route path={["/", "/login"]} exact component={ Login } />
                    <Route path="/translate" component={ Translate } />
                    <Route path="/profile" component={ Profile } />
                    <Route path="*" component={ NotFound } />
                </Switch>
            </div>
        </BrowserRouter>
    );
}

export default App;
