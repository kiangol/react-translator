import {
    ACTION_ADD_TRANSLATE_ATTEMPTING,
    ACTION_ADD_TRANSLATE_SUCCESS,
    addTranslateSuccessAction,
} from "../actions/translateActions";
import {TranslateAPI} from "../../components/Translate/TranslateAPI";

export const loginMiddleware = ({dispatch}) => next => action => {

    next(action)
    if (action.type === ACTION_ADD_TRANSLATE_ATTEMPTING) {
        // Make HTTP request to try register a user
        // If the user already exists, server is going to
        // respond with an error
        // Then we try to login instead
        TranslateAPI.addTranslation(action.payload)
            .then(profile => {
                // Successful registering
                dispatch(addTranslateSuccessAction(profile))
            })
    }

}