import {loginMiddleware} from "./loginMiddleware";
import {applyMiddleware} from "redux";
import {sessionMiddleware} from "./sessionMiddleware";
import {userMiddleware} from "./userMiddleware";

export default applyMiddleware(
    loginMiddleware,
    sessionMiddleware,
    userMiddleware
)