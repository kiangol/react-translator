import {ACTION_SESSION_INIT, ACTION_SESSION_SET, ACTION_SESSION_CLEAR, ACTION_SESSION_LOGOUT, sessionSetAction, sessionClearAction} from "../actions/sessionActions";

export const sessionMiddleware = ({ dispatch }) => next => action => {
    next(action)

    if(action.type === ACTION_SESSION_INIT) {
        // Read localstorage and check if session exists
        const storedSession = localStorage.getItem('translateusr-ss')
        if(!storedSession) {
            return
        }
        const session = JSON.parse(storedSession)
        dispatch(sessionSetAction(session))
    }

    if(action.type === ACTION_SESSION_SET) {
        // Store the session in localstorage
        localStorage.setItem('translateusr-ss', JSON.stringify(action.payload))
    }

    if (action.type === ACTION_SESSION_CLEAR) {
        localStorage.removeItem('translateusr-ss')
    }

    if (action.type === ACTION_SESSION_LOGOUT) {
        dispatch(sessionClearAction())
    }
}