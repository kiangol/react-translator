import {
    ACTION_LOGIN_ATTEMPTING,
    ACTION_LOGIN_SUCCESS,
    loginErrorAction,
    loginSuccessAction
} from "../actions/loginActions";
import {LoginAPI} from "../../components/Login/LoginAPI";
import {sessionSetAction} from "../actions/sessionActions";

export const loginMiddleware = ({dispatch}) => next => action => {

    next(action)
    if (action.type === ACTION_LOGIN_ATTEMPTING) {
        // Make HTTP request to try register a user
        // If the user already exists, server is going to
        // respond with an error
        // Then we try to login instead
        LoginAPI.register(action.payload)
            .then(profile => {
                // Successful registering
                dispatch(loginSuccessAction(profile))
            })
            .catch(error => {
                // Error registering, log in instead
                LoginAPI.login(action.payload)
                    .then(profile => {
                        dispatch(loginSuccessAction(profile))
                    })

            })

    }

    if (action.type === ACTION_LOGIN_SUCCESS) {
        // Logged in, do something about it
        dispatch( sessionSetAction(action.payload))

    }

}