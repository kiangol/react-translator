import {ACTION_USER_FETCHING, userErrorAction, userSetAction} from "../actions/userActions";
import {ProfileAPI} from "../../components/Profile/ProfileAPI";

export const userMiddleware = ({dispatch}) => next => action => {
    next(action)

    if (action.type === ACTION_USER_FETCHING) {
        console.log(action)
        ProfileAPI.getLatestTranslations(action.payload)
            .then(({history}) => {
                dispatch(userSetAction(history))
            })
            .catch(({ message }) => {
                console.log(message)
                dispatch(userErrorAction(message))
            })
    }
}