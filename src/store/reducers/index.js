import {loginReducer} from "./loginReducer";
import {userReducer} from "./userReducer";
import {combineReducers} from "redux";
import {sessionReducer} from "./sessionReducer";

const appReducer = combineReducers({
    loginReducer,
    sessionReducer,
    userReducer
})

export default appReducer