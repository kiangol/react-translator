import {ACTION_SESSION_SET, ACTION_SESSION_LOGOUT, ACTION_SESSION_CLEAR } from "../actions/sessionActions";

const initialState = {
    username: "",
    history: [],
    loggedIn: false
}

export const sessionReducer = (state = initialState, action) => {
    switch(action.type) {
        case ACTION_SESSION_SET:
            console.log("LOGGED IN: " + {...action.payload.username})
            return {
                ...action.payload,
                loggedIn: true,
            }
        case ACTION_SESSION_CLEAR:
            return {
                ...initialState
            }
        case ACTION_SESSION_LOGOUT:
            console.log("LOGOUT: ")
            return {
                ...state,
                loggedIn: false
            }
        default:
            return state
    }
}