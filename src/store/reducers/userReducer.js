import {ACTION_USER_ERROR, ACTION_USER_FETCHING, ACTION_USER_SET} from "../actions/userActions";

const initialState = {
    userError: '',
    userFetching: false,
    userHistory: []
}

export const userReducer = (state = {...initialState}, action) => {
    switch(action.type) {
        case ACTION_USER_FETCHING:
            return {
                ...state,
                userError: '',
                userFetching: true,
            }
        case ACTION_USER_SET:
            return {
                userError: '',
                userFetching: false,
                userHistory: [...action.payload]
            }
        case ACTION_USER_ERROR:
            return {
                ...state,
                userError: '',
                userFetching: false
            }
        default:
            return state
    }
}