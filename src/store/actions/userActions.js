export const ACTION_USER_FETCHING = '[USER] FETCHING'
export const ACTION_USER_SET = '[USER] SET'
export const ACTION_USER_ERROR = '[USER] ERROR'

export const userFetchingAction = userId => ({
    type: ACTION_USER_FETCHING,
    payload: userId
})

export const userSetAction = history => ({
    type: ACTION_USER_SET,
    payload: history
})

export const userErrorAction = error => ({
    type: ACTION_USER_ERROR,
    payload: error
})