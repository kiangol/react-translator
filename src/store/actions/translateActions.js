export const ACTION_ADD_TRANSLATE_ATTEMPTING = '[add_translate] ATTEMPT'
export const ACTION_ADD_TRANSLATE_SUCCESS = '[add_translate] SUCCESS'
export const ACTION_ADD_TRANSLATE_ERROR = '[add_translate] ERROR'

export const addTranslateAttemptAction = credentials => ({
    type: ACTION_ADD_TRANSLATE_ATTEMPTING,
    payload: credentials
})

export const addTranslateSuccessAction = profile => ({
    type: ACTION_ADD_TRANSLATE_SUCCESS,
    payload: profile
})

export const addTranslateErrorAction = error => ({
    type: ACTION_ADD_TRANSLATE_ERROR,
    payload: error
})